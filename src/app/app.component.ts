import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import { Observable, Subscription, filter, map, mergeMap, of } from 'rxjs';

import { Store } from '@ngrx/store';
import { selectAllBooks } from './store/books.selectors';
import { Book } from './app-interfaces';

import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    // standalone: true,
    // imports: [NgIf, NgFor, MatSidenavModule, MatCheckboxModule, FormsModule, MatButtonModule],
})
export class AppComponent implements OnDestroy {
    // You only see it for fractions!
    title = '************************';

    snackbarSubscription: Subscription | undefined;

    // for our Store of BUYING books!
    numberOfBookItems$: Observable<Book[]> | undefined;

    constructor(
        private titleService: Title,

        private store: Store,
        private matsnackbar: MatSnackBar,

        private router: Router,
        private renderer: Renderer2,
        @Inject(DOCUMENT) private document: any,
        private activatedRoute: ActivatedRoute
    ) {
        this.titleService.setTitle(this.title);
        this.updateCart();
        this.updateRouter();
    }

    private updateCart() {
        this.numberOfBookItems$ = this.store.select(selectAllBooks);
    }

    private updateRouter() {
        const events = this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            // //
            .pipe(map(() => this.activatedRoute))
            .pipe(map((route) => {
                while (route.firstChild) {
                    route = route.firstChild;
                }
                return route;
            }))
            // //
            .pipe(mergeMap((route) => route.data))
            ;
        events.subscribe(data => this.updateBodyClass(data['bodyClass']));
    }

    private updateBodyClass(customBodyClass?: string) {
        this.renderer.setAttribute(this.document?.body, 'class', '');
        if (customBodyClass) {
            this.renderer.addClass(this.document?.body, customBodyClass);
        }
    }

    onItemAdded(book: Book, matsideNav: MatSidenav) {
        this.snackbarSubscription = this.matsnackbar.open(
            `${book.title} added to your cart.`, matsideNav.opened ? '' : 'Open Cart'
        )
        .onAction()
        .subscribe(() => matsideNav.open());
    }

    ngOnDestroy(): void {
        this.snackbarSubscription?.unsubscribe();
    }


}
