import { createAction, createActionGroup, props } from '@ngrx/store';
import { CartItem } from 'src/app/app-interfaces';

export const itemAddedSuccess = createAction(
    '[Cart/API] Item added to cart successfully'
);

export const CartPageActions = createActionGroup({
    source: 'Cart/Page',
    events: {
        'Decrease number of item in cart': props<{ basket: CartItem }>(),
        'Increase number of item in cart': props<{ basket: CartItem }>(),
        'Remove item from cart': props<{ basket: CartItem }>()
    }
})
