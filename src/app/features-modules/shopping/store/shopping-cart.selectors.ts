import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartItem } from '../../../app-interfaces';


// export const selectCartFeature = createFeatureSelector<CartFeatureState>('cartFeature');

// export const selectNumberOfCartItems = createSelector(
//     selectCartFeature,
//     (state: CartFeatureState) => state.numberOfItems
// );


export interface CartFeatureState {
    cartItems: CartItem[],
    qty: number
}

export const selectCartState = (state: CartFeatureState) => state;

export const selectCartItems = createSelector(
    selectCartState,
    (state: any | undefined) => {
        return state?.cartFeature?.cartItems;
    }
);

export const selectCartTotalPrice = createSelector(
    selectCartState,
    (state: any | undefined) => {
        return state.cartFeature?.cartItems.reduce((accumulator: number, cartItem: CartItem) => {
            return cartItem.item.saleInfo.listPrice ? accumulator + (cartItem.qty * cartItem.item.saleInfo.listPrice?.amount) : 0;
        }, 0);
    }
);
