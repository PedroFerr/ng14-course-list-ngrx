import { createReducer, on } from '@ngrx/store';
import { CartFeatureState } from './shopping-cart.selectors';
import { CartItem } from '../../../app-interfaces';
import { CatalogPageActions } from '../../../store/books.actions';
import { CartPageActions } from './shopping-cart.actions';

export const initialState: CartFeatureState = {
    cartItems: [],
    qty: 0
};

const getNumberOfItems = (cartItems: CartItem[]): number => {
    return cartItems.reduce((partialSum, cartItem) => partialSum + cartItem.qty, 0);
}

export const cartReducer = createReducer(
    initialState,
    on(CatalogPageActions.addItemToCart, (store: CartFeatureState, result) => {
        const existingItem = store.cartItems.find((id: any) => id === result.book.id);

        return {
            ...store,
            cartItems: store.cartItems.map((cartItem: any) => cartItem.item.id !== result.book.id
                ? cartItem
                : { ...cartItem, qty: cartItem.qty + 1 }
            ).concat(existingItem ? [] : [{ id: result.book.id, qty: 1, item: result.book.id }]),
            qty: store.qty + 1
        }
    }
    ),
    on(CartPageActions.decreaseNumberOfItemInCart, (store: CartFeatureState, result) => {
        return {
            ...store,
            cartItems: store.cartItems.map((cartItem: any) => cartItem.id !== result.basket.id
                ? cartItem
                : { ...cartItem, qty: cartItem.qty - 1 }
            ).filter((qty: number) => qty > 0),
            qty: store.qty - 1
        }
    }),
    on(CartPageActions.increaseNumberOfItemInCart, (store: CartFeatureState, result) => {
        return {
            ...store,
            cartItems: store.cartItems.map((cartItem: any) => cartItem.id !== result.basket.id
                ? cartItem
                : { ...cartItem, qty: cartItem.qty + 1 }
            ).filter((qty: number) => qty > 0),
            qty: store.qty + 1
        }
    }),
    on(CartPageActions.removeItemFromCart, (store: CartFeatureState, result) => {
        const cartItems = [...store.cartItems.filter((item: any) => item.id !== result.basket.id)];
        return {
            cartItems,
            qty: getNumberOfItems(cartItems)
        }
    }),
);

export { CartFeatureState };
