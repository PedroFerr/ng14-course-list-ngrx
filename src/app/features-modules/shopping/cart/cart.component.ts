import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CartFeatureState, selectCartItems, selectCartTotalPrice } from '../store/shopping-cart.selectors';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { CartItem } from '../../../app-interfaces';
import { CartPageActions } from '../store/shopping-cart.actions';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent {
    cartItems$: Observable<CartItem[]>;
    totalPrice$: Observable<number>;

    @Input() cartItem: CartItem | undefined;

    @Output() closeEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    id = 'id';

    constructor(private store: Store<CartFeatureState>) {
        this.cartItems$ = this.store.select(selectCartItems);
        this.totalPrice$ = this.store.select(selectCartTotalPrice);
    }

    decreaseItem(basket: CartItem) {
        this.store.dispatch(CartPageActions.decreaseNumberOfItemInCart({ basket }))
    }

    removeItem(basket: CartItem) {
        this.store.dispatch(CartPageActions.removeItemFromCart({ basket }));
    }

    increaseItem(basket: CartItem) {
        this.store.dispatch(CartPageActions.increaseNumberOfItemInCart({ basket }));
    }

    closeCart() {
        this.closeEvent.emit(true);
    }
}
