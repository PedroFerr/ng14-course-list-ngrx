import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

import { CartEffect } from './store/shopping-cart.effects';
import { StoreModule } from '@ngrx/store';
import { cartReducer } from './store/shopping-cart.reducer';

import { AppMaterialModule } from 'src/app/app-material.module';
import { NgPipesModule } from 'ngx-pipes';

import { CartComponent } from './cart/cart.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';



@NgModule({
    imports: [
        CommonModule,
        AppMaterialModule,
        EffectsModule.forFeature([CartEffect]),
        StoreModule.forFeature('shoppingCartFeature', cartReducer),
        NgPipesModule
    ],
    exports: [
      CartComponent,
      CartItemComponent
  ],
    declarations: [
        CartComponent,
        CartItemComponent
    ],
    providers: [],
})
export class CartModule { }
