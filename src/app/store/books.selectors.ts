import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromBooks from './books.reducer'

export const selectBooksState = createFeatureSelector<fromBooks.BookState>('books');

export const selectAllBooks = createSelector(
    selectBooksState,
    fromBooks.selectAll
);
