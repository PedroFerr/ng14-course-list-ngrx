import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';
import { Book } from 'src/app/app-interfaces';

export const getBooks = createAction(
    '[App Component] Request the books'
);

export const retrievedBooks = createAction(
    '[Book Service API Call] Get Books Successfully',
    props<{ books: Book[] }>()
);

export const CatalogPageActions = createActionGroup({
    source: 'Items/Page',
    events: {
        'Get Items': emptyProps(),
      'Add item to cart': props<{ book: Book }>(),
        'Open Cart': emptyProps()
    }
});
